from flask import Flask, make_response
from requests import get

# This function is what actually utilises the external web service.
from get_steam_data.get_steam_data import get_profile

app = Flask(__name__)


@app.route('/')
def hello_world():
    # The user provides as many Steam keys for as many profiles as they wish, which are put into a list.
    steam_keys = [input("Please enter a Steam key: ")]
    while True:
        key = input("Please enter another Steam key, or enter x to continue: ")
        if key == "x":
            break
        steam_keys.append(key)

    # Every Steam key is used in a call to the external web service to get the corresponding profile data.
    steam_ladder_data = []
    for key in steam_keys:
        steam_ladder_data.append(get_profile(key))

    # This data is then turned into strings and put into one big csv string.
    ladder_data_csv = ""
    for profile in steam_ladder_data:
        for i in range(0, len(profile)):
            ladder_data_csv += str(profile[i]) + ','

    # The final comma is cut off to avoid out of bounds errors.
    ladder_data_csv = ladder_data_csv[:-1]

    # The second web service is then called, being passed this csv of profile data to analyse it.
    r = get('http://localhost:5001/analysis/<ladder_data_csv>', data={'data': ladder_data_csv})

    # The result of the get call is only a response message of success or failure.
    # It must be put into a special function to get the actual output.
    # This output then has its format adjusted for the final web service.
    stats = r.text.strip().strip("\"")

    # call pdf rendering service to get pdf rendered in hex
    r = get('http://localhost:5002/', data={'data': stats})
    pdf_hex_str = r.text

    # transcode from hex string to binary, add headers and render
    pdf_bytes = bytes.fromhex(pdf_hex_str[1:-2])
    response = make_response(pdf_bytes)
    response.headers['Content-Type'] = "application/pdf"
    response.headers['Content-Disposition'] = "inline; filename=stats.pdf"

    return response


if __name__ == '__main__':
    app.run()
