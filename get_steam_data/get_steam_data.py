import requests

api_key = '7ea5306844e79e402b8ba9f36d1123ff5e7d053a'  # This API key is the authorisation key to accessing data.
base_url = 'https://steamladder.com/api/v1'             # This is the site which the program contacts.

def get_analysis_data(r):
    print("[GET] {}".format(r.url))

    # Assuming the retrieval was successful, the required data is extracted from the larger set.
    if r.status_code == 200:
        level = r.json()['steam_stats']['level']
        xp = r.json()['steam_stats']['xp']
        games = r.json()['steam_stats']['games']['total_games']
        playtime = r.json()['steam_stats']['games']['total_playtime_min']
        xp_rank = r.json()['ladder_rank']['worldwide_xp']
        games_rank = r.json()['ladder_rank']['worldwide_games']
        playtime_rank = r.json()['ladder_rank']['worldwide_playtime']

        return [level, xp, games, playtime, xp_rank, games_rank, playtime_rank]
    # There are a few reasons why it may not be successful.
    elif r.status_code == 429:
        print("Request rate limited: {}".format(r.text))
    elif r.status_code == 401:
        print("Not authorized: {}".format(r.text))
    elif r.status_code == 404:
        print("Not found: {}".format(r.text))

def get_profile(steam_id):
    # This contacts the Steam Ladder site and retrieves the profile data in json format.
    r = requests.get('{}/profile/{}/'.format(base_url, steam_id), headers={
        'Authorization': 'Token {}'.format(api_key)
    })
    return get_analysis_data(r)
