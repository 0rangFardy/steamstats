from print_to_pdf import print_to_pdf
from flask import make_response, Flask
from requests import get
import base64

app = Flask(__name__)

test_str_list = ["bad", "vibes", "are", "no", "good"]
#og_test_str

@app.route('/', methods=['GET', 'POST'])
def test_print_to_pdf():
    pdf_ting = print_to_pdf(test_str_list)

    print(pdf_ting)
    response = make_response(pdf_ting)
    response.headers['Content-Type'] = "application/pdf"
    response.headers['Content-Disposition'] = "inline; filename=stats.pdf"

    return response


@app.route('/restful')
def test_pdf_restful():
    test_str = "for,every,vibe,there,is,an,equal,and,opposite,anti,vibe"
    test_tuple = ('for', 'every', 'vibe', 'there', 'is', 'an', 'equal', 'and', 'opposite', 'anti', 'vibe')
    r = get('http://localhost:5002/', data={'data': test_str})

    pdf_ting = r.text
    print(pdf_ting)
    print(type(pdf_ting))
    print(pdf_ting[17400])
    pdf_bytes = bytes.fromhex(pdf_ting[1:-2])
    print(pdf_bytes)
    response = make_response(pdf_bytes)
    response.headers['Content-Type'] = "application/pdf"
    response.headers['Content-Disposition'] = "inline; filename=stats.pdf"

    return response


if __name__ == '__main__':
    app.run(debug=True)