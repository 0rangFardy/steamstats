from flask import Flask, request, render_template
from flask_restful import Resource, Api
import pdfkit

app = Flask(__name__)
api = Api(app)
value_names = ["Highest Level: ", "Lowest Level: ", "Average Level: ",
               "Highest XP: ", "Lowest XP: ", "Average XP: ",
               "Most Games: ", "Least Games: ", "Average Games: ",
               "Highest Playtime: ", "Lowest Playtime: ", "Average Playtime: ",
               "Highest XP Rank: ", "Lowest XP Rank: ", "Average XP Rank: ",
               "Highest Games Rank: ", "Lowest Games Rank: ", "Average Games Rank: ",
               "Highest Playtime Rank: ", "Lowest Playtime Rank: ", "Average Playtime Rank: "]

# wkhtmltopdf install path. change this to point to the executable on your machine
# alternatively, do not pass an argument and it will guess the path
# example: config = pdfkit.configuration(wkhtmltopdf=r"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe")
config = pdfkit.configuration()
options = {
    "enable-local-file-access": None
}


# returns a rendered pdf in bytes
# vals : the list of values to render in the pdf
def print_to_pdf(vals):
    for i in range(len(value_names)):
        value_names[i] += vals[i]
    html = render_template("player_stats_analysis.html", stats=value_names)
    pdf = pdfkit.from_string(html, False, configuration=config, options=options)
    return pdf


class PrintToPdf(Resource):
    # on a http call, produce a pdf using values passed in csv format
    # send pdf data back as a hex string
    def get(self):
        csv_in = request.form['data']
        out = print_to_pdf(csv_in.split(','))
        return out.hex()


api.add_resource(PrintToPdf, '/')

if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port=5002)
