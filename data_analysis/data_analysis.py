from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

def analysis(profile_data):
    # All the data to be analysed
    high_lvl = 0
    low_lvl = 999999
    avg_lvl = 0
    high_xp = 0
    low_xp = 999999999
    avg_xp = 0
    most_games = 0
    least_games = 999999
    avg_games = 0
    high_time = 0
    low_time = 999999999
    avg_time = 0
    high_xp_rank = 0
    low_xp_rank = 999999999
    avg_xp_rank = 0
    high_games_rank = 0
    low_games_rank = 999999999
    avg_games_rank = 0
    high_time_rank = 0
    low_time_rank = 999999999
    avg_time_rank = 0
    private_profiles = 0

    # Every value in the provided data is turned from a string to an int.
    for i in range(len(profile_data)):
        profile_data[i] = int(profile_data[i])

    for i in range (0, len(profile_data)-1, 7):
        # Analysis for the profile's level.
        level = profile_data[i]
        if level > high_lvl:
            high_lvl = level
        if level < low_lvl:
            low_lvl = level
        avg_lvl += level

        # Analysis for the profile's xp.
        xp = profile_data[i+1]
        if xp > high_xp:
            high_xp = xp
        if xp < low_xp:
            low_xp = xp
        avg_xp += xp

        # Analysis for the profile's games number.
        games = profile_data[i+2]
        if games != 0:
            if games > most_games:
                most_games = games
            if games < least_games:
                least_games = games
            avg_games += games
        else:
            private_profiles += 1

        # Analysis for the profile's playtime.
        playtime = profile_data[i+3]
        if playtime != 0:
            if playtime > high_time:
                high_time = playtime
            if playtime < low_time:
                low_time = playtime
            avg_time += playtime
        else:
            private_profiles += 1

        # Analysis for the profile's worldwide ladder rank for xp.
        xp_rank = profile_data[i+4]
        if xp_rank > high_xp_rank:
            high_xp_rank = xp_rank
        if xp_rank < low_xp_rank:
            low_xp_rank = xp_rank
        avg_xp_rank += xp_rank

        # Analysis for the profile's worldwide ladder rank for games number.
        games_rank = profile_data[i+5]
        if games_rank > high_games_rank:
            high_games_rank = games_rank
        if games_rank < low_games_rank:
            low_games_rank = games_rank
        avg_games_rank += games_rank

        # Analysis for the profile's worldwide ladder rank for playtime.
        time_rank = profile_data[i+6]
        if time_rank > high_time_rank:
            high_time_rank = time_rank
        if time_rank < low_time_rank:
            low_time_rank = time_rank
        avg_time_rank += time_rank

    # Averages are calculated, accounting for private accounts withholding data.
    avg_lvl /= len(profile_data)/7
    avg_xp /= len(profile_data)/7
    avg_games /= len(profile_data)/7 - private_profiles
    avg_time /= len(profile_data)/7 - private_profiles
    avg_xp_rank /= len(profile_data)/7
    avg_games_rank /= len(profile_data)/7
    avg_time_rank /= len(profile_data)/7

    # If all the accounts were private, the withheld data is marked as such.
    if most_games == 0:
        most_games = "no data"
    if least_games == 999999:
        least_games = "no data"
    if avg_games == 0:
        avg_games = "no data"
    if high_time == 0:
        high_time = "no data"
    if low_time == 999999999:
        low_time = "no data"
    if avg_time == 0:
        avg_time = "no data"

    # A big string is made of all the final values concatenated with commas, like a csv.
    output_csv = str(high_lvl) + "," + str(low_lvl) + "," + str(int(avg_lvl)) + "," + str(high_xp) + "," + str(low_xp) \
                 + "," + str(int(avg_xp)) + "," + str(most_games) + "," + str(least_games) + "," + str(avg_games) \
                 + "," + str(high_time) + "," + str(low_time) + "," + str(avg_time) + "," + str(high_xp_rank) + \
                 "," + str(low_xp_rank) + "," + str(int(avg_xp_rank)) + "," + str(high_games_rank) + "," + \
                 str(low_games_rank) + "," + str(int(avg_games_rank)) + "," + str(high_time_rank) + "," + \
                 str(low_time_rank) + "," + str(int(avg_time_rank))

    return output_csv

class DataAnalysis(Resource):
    # No values are changed on the server end, so only a get is needed.
    def get(self, ladder_data_csv):
        # The provided data is a csv string. This is split into an array of strings, to be passed to analysis().
        profile_data = request.form['data'].split(",")
        return analysis(profile_data)

# The use of <> marks allows the provided data to form part of the url, which is necessary for it to be passed.
api.add_resource(DataAnalysis, '/analysis/<ladder_data_csv>')

if __name__ == '__main__':
    app.run(debug=True)