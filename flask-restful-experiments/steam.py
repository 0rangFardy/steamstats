from flask import Flask
from flask_restful import Resource, Api
import requests

app = Flask(__name__)
api = Api(app)

api_key = '7ea5306844e79e402b8ba9f36d1123ff5e7d053a'  # Your API key here
base_url = 'https://steamladder.com/api/v1'
steam_id = '76561198037767629'  # The SteamID64 you want to lookup here

r = requests.get('{}/profile/{}/'.format(base_url, steam_id), headers={
        'Authorization': 'Token {}'.format(api_key)
    })

def get_worldwide_xp_rank(r):
    print("[GET] {}".format(r.url))
    if r.status_code == 200:
        worldwide_xp_rank = r.json()['ladder_rank']['worldwide_xp']
        print("Worldwide XP rank: {}".format(worldwide_xp_rank))
    elif r.status_code == 429:
        print("Request rate limited: {}".format(r.text))
    elif r.status_code == 401:
        print("Not authorized: {}".format(r.text))
    elif r.status_code == 404:
        print("Not found: {}".format(r.text))

    print(r.json())

def get_country_xp_rank(r):
    print("[GET] {}".format(r.url))
    if r.status_code == 200:
        country = r.json()['steam_user']['steam_country_code']
        country_xp_rank = r.json()['ladder_rank']['country']['country_xp']
        print("{} XP rank: {}".format(country, country_xp_rank))
    elif r.status_code == 429:
        print("Request rate limited: {}".format(r.text))
    elif r.status_code == 401:
        print("Not authorized: {}".format(r.text))
    elif r.status_code == 404:
        print("Not found: {}".format(r.text))

def get_steam_level(r):
    print("[GET] {}".format(r.url))
    if r.status_code == 200:
        steam_level = r.json()['steam_stats']['level']
        print("Level: {}".format(steam_level))
    elif r.status_code == 429:
        print("Request rate limited: {}".format(r.text))
    elif r.status_code == 401:
        print("Not authorized: {}".format(r.text))
    elif r.status_code == 404:
        print("Not found: {}".format(r.text))

def get_analysis_data(r):
    print("[GET] {}".format(r.url))
    if r.status_code == 200:
        level = r.json()['steam_stats']['level']
        print("Level: {}".format(level))
        xp = r.json()['steam_stats']['xp']
        print("xp: {}".format(xp))
        games = r.json()['steam_stats']['games']['total_games']
        if games == 0:
            print("Games not available, this profile's library is private.")
        else:
            print("Games: {}".format(games))
        playtime = r.json()['steam_stats']['games']['total_playtime_min']
        if playtime == 0:
            print("Playtime not available, this profile's library is private.")
        else:
            print("Playtime: {}".format(playtime))
        xp_rank = r.json()['ladder_rank']['worldwide_xp']
        print("Worldwide XP rank: {}".format(xp_rank))
        games_rank = r.json()['ladder_rank']['worldwide_games']
        print("Worldwide Games rank: {}".format(games_rank))
        playtime_rank = r.json()['ladder_rank']['worldwide_playtime']
        print("Worldwide Playtime rank: {}".format(playtime_rank))
    elif r.status_code == 429:
        print("Request rate limited: {}".format(r.text))
    elif r.status_code == 401:
        print("Not authorized: {}".format(r.text))
    elif r.status_code == 404:
        print("Not found: {}".format(r.text))

class getSteam(Resource):
    def get(self, steam_id):
        r = requests.get('{}/profile/{}/'.format(base_url, steam_id), headers={
            'Authorization': 'Token {}'.format(api_key)
        })
        get_analysis_data(r)
        return {steam_id: r.json()}

api.add_resource(getSteam, '/profile/<string:steam_id>')

if __name__ == '__main__':
    app.run(debug=False)