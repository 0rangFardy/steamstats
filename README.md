# SteamStats


# Guide for Installing and Running
- We have only had the time to test this application in a Pycharm Professional Flask Environment, so this guide assumes that you have access to this as well
- To run this application it is also necessary to have wkhtmltopdf installed to your machine: https://wkhtmltopdf.org/downloads.html
- You may also need to pass an argument in [this line](https://gitlab.com/0rangFardy/steamstats/-/blob/main/print_to_pdf/print_to_pdf.py#L18) of print_to_pdf/print_to_pdf.py denoting the path to the installed executable
- Clone the repository to your machine. 
- Create a new project in Pycharm using the Flask project template. Set the root of this project to be the path to your cloned repository. 
- You will need to run three Flask applications simultaneously, each with its own unique port number. We have generated xml run configuration files in the .run directory to make this more streamlined. You should have [print_to_pdf.py](https://gitlab.com/0rangFardy/steamstats/-/blob/main/print_to_pdf/print_to_pdf.py) and [data_analysis.py](https://gitlab.com/0rangFardy/steamstats/-/blob/main/data_analysis/data_analysis.py) already running before you try to run [app.py](https://gitlab.com/0rangFardy/steamstats/-/blob/main/app.py)
- Once you finally run app.py, follow the link to open http://localhost/5000 in your browser, then go back to the run console and pass in some Steam IDs. Here are ours you can use:
    - 76561198037767629
    - 76561198153500399
    - You can find more publicly available keys on steamladder.com if you wish to.
- Enter "x" into the console once you have finished entering IDs, and go back to your browser. 
- You should hopefully see a group analysis showing mean, max and min statistics for the steam profiles you passed, all rendered in a downloadable, printable pdf :)
